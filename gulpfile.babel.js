//Constantes para el automatizador de tareas
const gulp = require("gulp")
const sass = require("gulp-sass")
const babel = require("gulp-babel")
const autoprefixer = require("gulp-autoprefixer")
const concat = require("gulp-concat")
const uglify = require("gulp-uglify")
const plumber = require("gulp-plumber")

//Constante para el modulo de recarga automática del sitio web al hacer cambios
const browserSync = require('browser-sync')

//Instancia del servidor de desarrollo
const server = browserSync.create()

//tarea para los estilos de la UX
gulp.task("stylesUx", () => {
  return gulp
    .src('./src/scss/styles.scss')
    .pipe(plumber())
    .pipe(
      sass({
        outputStyle: "compact"
      })
    )
    .pipe(
      autoprefixer({
        browsers: ["last 3 versions"]
      })
    )
    .pipe(gulp.dest('./public/ux/css'))
    .pipe(server.stream())
})

//tarea para los estilos del admin
gulp.task("stylesAdmin", () => {
  return gulp
    .src('./src/scss/styles.scss')
    .pipe(plumber())
    .pipe(
      sass({
        outputStyle: "compact"
      })
    )
    .pipe(
      autoprefixer({
        browsers: ["last 3 versions"]
      })
    )
    .pipe(gulp.dest('./public/admin/css'))
    .pipe(server.stream())
})

//tarea para los estilos de la web principal
gulp.task("stylesWeb", () => {
  return gulp
    .src('./src/scss/styles.scss')
    .pipe(plumber())
    .pipe(
      sass({
        outputStyle: "compact"
      })
    )
    .pipe(
      autoprefixer({
        browsers: ["last 3 versions"]
      })
    )
    .pipe(gulp.dest('./public/web/css'))
    .pipe(server.stream())
})

//tarea para el js de la UX
gulp.task("babelUx", () => {
  return gulp
    .src("./src/js/ux/*.js")
    .pipe(plumber())
    .pipe(
      babel({ presets: ["@babel/preset-env"] })
    )
    .pipe(concat("scripts-min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./public/ux/js/"))
})


//tarea para el js del admin
gulp.task("babelAdmin", () => {
  return gulp
    .src("./src/js/admin/*.js")
    .pipe(plumber())
    .pipe(
      babel({ presets: ["@babel/preset-env"] })
    )
    .pipe(concat("scripts-min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./public/admin/js/"))
})

//tarea para el js de la web principal
gulp.task("babelWeb", () => {
  return gulp
    .src(["./src/js/web/helpers/*.js", "./src/js/web/*.js"])
    .pipe(plumber())
    .pipe(
      babel({
        presets: ["@babel/preset-env"]
      })
    )
    .pipe(concat("scripts-min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./public/web/js/"))
})

//tarea por defecto para que se ejecuten todas
gulp.task('default', () => {
  //Iniciación del servidor en el puerto 80
  server.init({
    proxy: 'localhost'
  })

  //Watchers (vigilantes) para vigilar los cambios y mostrarlos en tiempo real
  //SCSS
  gulp.watch('./src/scss/**/*.scss', gulp.series('stylesUx'))
  gulp.watch('./src/scss/**/*.scss', gulp.series('stylesAdmin'))
  gulp.watch('./src/scss/**/*.scss', gulp.series('stylesWeb'))

  //PUG
  gulp.watch('./src/views/ux/**').on('change', server.reload)
  gulp.watch('./src/views/admin/**').on('change', server.reload)
  gulp.watch('./src/views/web/**').on('change', server.reload)

  //JS
  gulp.watch("./src/js/ux/*.js", gulp.series('babelUx')).on('change', server.reload)
  gulp.watch("./src/js/admin/*.js", gulp.series('babelAdmin')).on('change', server.reload)
  gulp.watch("./src/js/web/helpers/*.js", gulp.series('babelWeb')).on('change', server.reload)
  gulp.watch("./src/js/web/*.js", gulp.series('babelWeb')).on('change', server.reload)
})