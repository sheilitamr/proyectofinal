const connection = require('../../dbConnection/connection')
const conn = connection()
// Es un objeto en el cual se crean las funciones que se ejecutan a través del enrutador.
const controller = {}

//Cada función comprueba si existe un usuario, si no existe renderiza el pug si enviar nada envía, si existe envía como dato el nombre de usuario

//Galería de inicio
controller.getHomeGallery = (req, res, next) => {
    //Hacemos una consulta a la base de datos
    conn.query("SELECT * FROM images", (err, rows) => {
        //Si hay error en la consulta lanzamos un error
        if (err) next(new Error(err));
        else res.send(rows)
    })
}

controller.getImageInfo = (req, res, next) => {
    //Hacemos una consulta a la base de datos
    conn.query("SELECT * FROM images WHERE id= ?", [req.params.id], (err, rows) => {
        //Si hay error en la consulta lanzamos un error
        if (err) next(new Error(err));
        //Si no mandamos el resultado de la consulta. Ponemos [0] para que devuelva el objeto directamente, no el array con una sola posición
        else res.send(rows[0])
    })
}

module.exports = controller