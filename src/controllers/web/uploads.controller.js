const connection = require('../../dbConnection/connection')
const conn = connection()
const controller = {}
const path = require('path')
const fs = require('fs-extra')

//Insertar en la foto en la base de datos
controller.uploads = (req, res, next) => {
    //Como en el paso de subir la foto no tenemos el usuario, necesitamos poner la foto en una carpeta temporal y después moverla a su carpeta correspondiente.

    //Ruta de donde está la imagen
    const srcpath = path.join(__dirname, `../../tmpUploads/${req.file.filename}`)
    //Ruta de donde queremos mover la imagen
    const dstpath = path.join(__dirname, `../../../public/uploads/${req.params.username}/${req.body.title}${path.extname(req.file.originalname)}`)

    //mover imagen (inicio, destino, si existe la borro, error)
    fs.move(srcpath, dstpath, { overwrite: true }, err => {
        if (err) next(new Error(err))
        //Si no hay error
        else {
            //Datos para la base de datos
            const data = {
                path: `/uploads/${req.params.username}/${req.body.title}${path.extname(req.file.originalname)}`,
                title: req.body.title,
                description: req.body.description,
                user: req.params.username,
                tag: req.body.tag.toLowerCase()
            }
            //inserción en la base de datos
            conn.query("INSERT INTO images SET ?", [data], (err, rows) => {
                if (err) next(new Error(err));
            })

            //Comprobamos si la etiqueta existe en la base de datos
            conn.query("SELECT * FROM tags WHERE tagName = ?", [data.tag], (err, rows) => {
                if (err) next(new Error(err));
                else {
                    if (rows.length == 0) {
                        conn.query("INSERT INTO tags SET tagName = ?", [data.tag], (err, rows) => {
                            if (err) next(new Error(err));
                        })
                    }
                }
            })
            res.redirect(`/web/${req.params.username}/profile/${req.params.username}`)
        }
    })
}

module.exports = controller