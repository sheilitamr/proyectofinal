const connection = require('../../dbConnection/connection')
const conn = connection()
const fs = require('fs')
const path = require('path')
// Es un objeto en el cual se crean las funciones que se ejecutan a través del enrutador.
const controller = {}

//Cada función comprueba si existe un usuario, si no existe renderiza el pug si enviar nada envía, si existe envía como dato el nombre de usuario

controller.getPosts = (req, res, next) => {
    conn.query('SELECT * FROM forum WHERE blog = ?', [req.params.blogId], (err, rows) => {
        if (err) next(new Error(err))
        else res.send(rows)
    })
}

controller.newPost = (req, res, next) => {
    const dataPost = {
        user: req.params.username,
        title: req.body.title,
        text: req.body.post,
        date: new Date().toLocaleString()
    }
    conn.query("INSERT INTO users SET ?", [dataUser], (err, rows) => {
        //Si hay un error en la inserción lanzamos el error
        if (err) next(new Error(err))
        else {

        }
    })
}

module.exports = controller