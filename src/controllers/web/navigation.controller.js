const connection = require('../../dbConnection/connection')
const conn = connection()
// Es un objeto en el cual se crean las funciones que se ejecutan a través del enrutador.
const controller = {}

//Cada función comprueba si existe un usuario, si no existe renderiza el pug si enviar nada envía, si existe envía como dato el nombre de usuario

//HOME
controller.home = (req, res, next) => {
    conn.query("SELECT tagName FROM tags", (err, rows) => {
        console.log(rows)
        //Si hay error en la consulta lanzamos un error
        if (err) next(new Error(err))
        else {
            res.charset = "utf-8"
            if (req.params.username == undefined) res.render('web/pages/home', { allTags: rows })
            else res.render('web/pages/home', { user: req.params.username, allTags: rows })
        }
    })
}

//CONTESTS
controller.contest = (req, res, next) => {
    if (req.params.username == undefined) res.render('web/pages/contest')
    else res.render('web/pages/contest', { user: req.params.username })
}

//BLOG
controller.blog = (req, res, next) => {
    conn.query("SELECT * FROM blogs", (err, rows) => {
        if (err) next(new Error(err))
        else {
            if (req.params.username == undefined) res.render('web/pages/blog', { data: rows })
            else res.render('web/pages/blog', { user: req.params.username, data: rows })
        }
    })

}

//ABOUT
controller.about = (req, res, next) => {
    if (req.params.username == undefined) res.render('web/pages/about')
    else res.render('web/pages/about', { user: req.params.username })
}

//CONTACT
controller.contact = (req, res, next) => {
    if (req.params.username == undefined) res.render('web/pages/contact')
    else res.render('web/pages/contact', { user: req.params.username })
}

//Exportamos el objeto controller para poder usarlo en otros archivos.
module.exports = controller