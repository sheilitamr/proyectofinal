const connection = require('../../dbConnection/connection')
const conn = connection()
const fs = require('fs')
const path = require('path')
// Es un objeto en el cual se crean las funciones que se ejecutan a través del enrutador.
const controller = {}

//Cada función comprueba si existe un usuario, si no existe renderiza el pug si enviar nada envía, si existe envía como dato el nombre de usuario

//Iniciar sesión
controller.login = (req, res, next) => {
    //Hacemos una consulta a la base de datos para saber si existe un registro con username y password
    conn.query("SELECT username, password FROM users WHERE username = ? AND password = ?",
        [req.body.username, req.body.password], (err, rows) => {
            //Si hay error en la consulta lanzamos un error
            if (err) next(new Error(err));
            else {
                res.charset = "utf-8"
                //Si hay registros significa que existe el usuario
                if (rows.length > 0) {
                    //Redirigimos a home con el usuario logueado
                    res.redirect(`/web/${req.body.username}/home#login`)
                }
                //Si no existe el usuario mostramos mensaje de error
                else res.redirect(`/web/home#error`)
            }
        })
}

//Registrar usuario
controller.register = (req, res, next) => {
    //Creamos un objeto con los datos del nuevo usuario
    const dataUser = {
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        image: '/web/img/user-profile.jpg'
    }

    //Hacemos la inserción
    conn.query("INSERT INTO users SET ?", [dataUser], (err, rows) => {
        //Si hay un error en la inserción lanzamos el error
        if (err) next(new Error(err))
        else {
            //Creamos una carpeta con el nombre del usuario
            fs.mkdir(path.join(__dirname, `../../../public/uploads/${req.body.username}`), { recursive: true }, (err) => {
                //Si algo va mal lanzamos el error
                if (err) next(new Error(err))
            })
            //Creamos una carpeta dentro de la carpeta de usuario para las fotos que no tengan galería
            fs.mkdir(path.join(__dirname, `../../../public/uploads/${req.body.username}/unknow`), { recursive: true }, (err) => {
                //Si algo va mal lanzamos el error
                if (err) next(new Error(err))
            })
        }
        //Redirigimos al home con el mensaje de usuario registrado
        res.redirect(`/web/home#registered`)
    })
}

//Cerrar sesión
controller.closeSession = (req, res, next) => {
    console.log('EXIT')
    //Redirigimos a inicio
    res.redirect(`/web/home#close`)
}

// Mostrar perfil
controller.userProfile = (req, res, next) => {
    conn.query(
        //La primera consulta es para obtener todas las imágenes del usuario que estamos visitando
        //La segunda consulta es para obtener los datos del perfil de usuario
        `SELECT galleries.id, galleries.title, galleries.user,
        galleries.description, galleries.tag, count(*) AS 'numImages'
        FROM galleries JOIN images
        ON galleries.id = images.gallery
        WHERE galleries.user = ?
        GROUP BY galleries.id, galleries.title, galleries.user, galleries.description, galleries.tag;   
        SELECT * FROM users WHERE username = ?;
        SELECT COUNT(*) AS totalImages FROM images WHERE user = ?`,
        [req.params.userProfile, req.params.userProfile, req.params.userProfile],
        (err, rows) => {
            //Si hay error en la consulta lanzamos un error
            if (err) next(new Error(err));
            else {
                res.charset = "utf-8"
                //Redirigimos a home con el usuario logueado
                res.render('web/pages/profile', {
                    user: req.params.user,
                    userProfile: req.params.userProfile,
                    galleries: rows[0],
                    userData: rows[1],
                    totalImages: rows[2].totalImages
                })
            }

            console.log(rows)
        })
}

controller.getUserGalleries = (req, res, next) => {
    conn.query('SELECT * FROM images WHERE user = ?', [req.params.userProfile], (err, rows) => {
        if (err) next(new Error(err))
        else res.send(rows)
    })
}

controller.editProfile = (req, res, next) => {
    console.log(req.params.userProfile)
    console.log(req.body)

    conn.query('UPDATE users SET ? WHERE username = ?', [req.body, req.params.username], (err, rows) => {
        if (err) next(new Error(err))
        else res.redirect(`/web/${req.params.username}/profile/${req.params.username}`)
    })
}

module.exports = controller