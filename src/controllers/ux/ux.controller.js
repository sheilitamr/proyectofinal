// Es un objeto en el cual se crean las funciones que se ejecutan a través del enrutador.
const controller = {}

//función que se llama desde el ux.router.js para controlar el acceso a home
controller.home = (req, res) => {
    //la respuesta utiliza el método render para renderizar. Como parámetro recibe la ruta que va a renderizar

    //Se usan los archivos template.pug, home.pug, colors.scss..... 

    res.render('ux/pages/home')
}

//función que se llama desde el ux.router.js para controlar el acceso a colors
controller.colors = (req, res) => {
    res.render('ux/pages/colors')
}

//función que se llama desde el ux.router.js para controlar el acceso a tipograhy
controller.tipography = (req, res) => {
    res.render('ux/pages/tipography')
}

//función que se llama desde el ux.router.js para controlar el acceso a buttons
controller.buttons = (req, res) => {
    res.render('ux/pages/buttons')
}

//función que se llama desde el ux.router.js para controlar el acceso a cards
controller.cards = (req, res) => {
    res.render('ux/pages/cards')
}

//función que se llama desde el ux.router.js para controlar el acceso a lists
controller.lists = (req, res) => {
    res.render('ux/pages/lists')
}

//función que se llama desde el ux.router.js para controlar el acceso a forms
controller.forms = (req, res) => {
    res.render('ux/pages/forms')
}

//función que se llama desde el ux.router.js para controlar el acceso a tabs
controller.tabs = (req, res) => {
    res.render('ux/pages/tabs')
}

//función que se llama desde el ux.router.js para controlar el acceso a galleries
controller.galleries = (req, res) => {
    res.render('ux/pages/galleries')
}

//función que se llama desde el ux.router.js para controlar el acceso a icons
controller.icons = (req, res) => {
    res.render('ux/pages/icons')
}

//función que se llama desde el ux.router.js para controlar el acceso a chat
controller.chat = (req, res) => {
    res.render('ux/pages/chat')
}

//Exportamos el objeto controller para poder usarlo en otros archivos.
module.exports = controller