const connection = require('../../dbConnection/connection')
const conn = connection()
// Es un objeto en el cual se crean las funciones que se ejecutan a través del enrutador.
const controller = {}

//función que recibe dos parámetros petición y respuesta
controller.home = (req, res) => {
    //la respuesta utiliza el método render para renderizar.Como parámetro recibe la ruta que va a renderizar
    res.render('admin/pages/home')
}

controller.users = (req, res) => {
    conn.query("SELECT * FROM users", (err, rows) => {
        if (err) next(new Error(err))
        else res.render('admin/pages/users', { data: rows })
    })
}

controller.blog = (req, res, next) => {
    conn.query("SELECT * FROM blogs", (err, rows) => {
        console.log(rows)
        if (err) next(new Error(err))
        else res.render('admin/pages/blog', { data: rows })
    })
}

controller.contests = (req, res) => {
    res.render('admin/pages/contests')
}


controller.deleteUser = (req, res, next) => {
    console.log(req.params.userId)
    conn.query("DELETE FROM users WHERE id= ?", [req.params.userId], (err, rows) => {
        if (err) next(new Error(err))
        else res.redirect('/admin/users')
    })
}

controller.insertBlog = (req, res, next) => {
    const dataBlog = {
        title: req.body.title,
        image: `/uploads/admin/${req.file.filename}`,
        description: req.body.content
    }

    //Hacemos la inserción
    conn.query("INSERT INTO blogs SET ?", [dataBlog], (err, rows) => {
        //Si hay un error en la inserción lanzamos el error
        if (err) next(new Error(err))
        else {
            //Redirigimos al home con el mensaje de usuario registrado
            res.redirect(`/admin/blog`)
        }
    })
}

controller.deleteBlog = (req, res, next) => {
    conn.query("DELETE FROM blogs WHERE id= ?", [req.params.blogId], (err, rows) => {
        if (err) next(new Error(err))
        else res.redirect('/admin/blog')
    })
}

//Exportamos el objeto controller para poder usarlo en otros archivos.
module.exports = controller