// Es un objeto en el cual se crean las funciones que se ejecutan a través del enrutador.
const controller = {}

//función que recibe dos parámetros petición y respuesta
controller.index = (req, res) => {
    //la respuesta utiliza el método render para renderizar.Como parámetro recibe la ruta que va a renderizar
    res.render('index')
}

controller.slide = (req, res) => {
    //la respuesta utiliza el método render para renderizar.Como parámetro recibe la ruta que va a renderizar
    res.render('slide')
}

module.exports = controller