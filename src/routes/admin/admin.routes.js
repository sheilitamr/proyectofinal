//Importa el módulo express.
const express = require('express')
//Guardamos el objeto que devuelve el método router. Router sirve para escuchar peticiones y enrutarlas.
const router = express.Router()

const uid = require('uid')

// El objeto controller es lo que devuelve el index.controller y se guarda en controller. Si no tuviera el module.exports no se podría usar.
const controller = require("../../controllers/admin/admin.controller")

router.get('/home', controller.home)

router.get('/users', controller.users)
router.get('/contests', controller.contests)
router.get('/blog', controller.blog)

router.get('/deleteUser/:userId', controller.deleteUser)

const path = require('path')
//Modulo para subir imágenes
const multer = require('multer')

//Opciones para guardar la foto
const storage = multer.diskStorage({
    //Destino de la foto
    destination: path.join(__dirname, '../../../public/uploads/admin'),
    //Nombre de la foto (nombre original - id unico , extensión de la foto)
    filename: (req, file, cb) => {
        cb(null, `${path.basename(file.originalname, path.extname(file.originalname))}-${uid(4)}-${path.extname(file.originalname)}`)
    }
})

const upload = multer({ storage })

router.post('/insertBlog', upload.single('blog-input'), controller.insertBlog)

router.get('/deleteBlog/:blogId', controller.deleteBlog)
module.exports = router