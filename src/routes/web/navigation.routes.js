//Importa el módulo express.
const express = require('express')

//Guardamos el objeto que devuelve el método router. Router sirve para escuchar peticiones y enrutarlas.
const router = express.Router()

// El objeto controller es lo que devuelve el index.controller y se guarda en controller. Si no tuviera el module.exports no se podría usar.
const controller = require("../../controllers/web/navigation.controller")

//no register
router.get('/home', controller.home)
router.get('/contest', controller.contest)
router.get('/blog', controller.blog)
router.get('/about', controller.about)
router.get('/contact', controller.contact)
//register
router.get('/:username/home', controller.home)
router.get('/:username/contest', controller.contest)
router.get('/:username/blog', controller.blog)
router.get('/:username/about', controller.about)
router.get('/:username/contact', controller.contact)

module.exports = router