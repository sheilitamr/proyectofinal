//Importa el módulo express.
const express = require('express')

//Guardamos el objeto que devuelve el método router. Router sirve para escuchar peticiones y enrutarlas.
const router = express.Router()

// El objeto controller es lo que devuelve el index.controller y se guarda en controller. Si no tuviera el module.exports no se podría usar.
const controller = require("../../controllers/web/user.controller")

router.post('/login', controller.login)
router.post('/register', controller.register)
router.get('/closeSession', controller.closeSession)
router.get('/:user/profile/:userProfile', controller.userProfile)
router.get('/profile/:userProfile', controller.userProfile)
router.get('/getUserGalleries/:userProfile', controller.getUserGalleries)

router.post('/editProfile/:username', controller.editProfile)

module.exports = router