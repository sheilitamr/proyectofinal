//Importa el módulo express.
const express = require('express')
//Guardamos el objeto que devuelve el método router. Router sirve para escuchar peticiones y enrutarlas.
const router = express.Router()

const path = require('path')
//Modulo para subir imágenes
const multer = require('multer')

const controller = require("../../controllers/web/uploads.controller")

//En este paso solo subimos la foto a una carpeta temporal, como no teneemos el nombre del usuario, moveremos la foto a la carpeta del usuario en el siguiente paso (uploads.controller)

//Opciones para guardar la foto
const storage = multer.diskStorage({
    //Destino de la foto
    destination: path.join(__dirname, '../../tmpUploads/'),
    //Nombre de la foto (nombre original - id unico , extensión de la foto)
    filename: (req, file, cb) => {
        cb(null, `${path.basename(file.originalname, path.extname(file.originalname))}-${path.extname(file.originalname)}`)
    }
})

const upload = multer({ storage })

router.post('/upload/:username', upload.single('file-input'), controller.uploads)

module.exports = router



