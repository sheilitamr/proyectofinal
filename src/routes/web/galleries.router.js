//Importa el módulo express.
const express = require('express')

//Guardamos el objeto que devuelve el método router. Router sirve para escuchar peticiones y enrutarlas.
const router = express.Router()

// El objeto controller es lo que devuelve el index.controller y se guarda en controller. Si no tuviera el module.exports no se podría usar.
const controller = require("../../controllers/web/galleries.controller")

router.get('/getHomeGallerie', controller.getHomeGallery)
router.get('/getImageInfo/:id', controller.getImageInfo)

module.exports = router