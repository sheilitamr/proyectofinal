//Importa el módulo express.
const express = require('express')

//Guardamos el objeto que devuelve el método router. Router sirve para escuchar peticiones y enrutarlas.
const router = express.Router()

// El objeto controller es lo que devuelve el index.controller y se guarda en controller. Si no tuviera el module.exports no se podría usar.
const controller = require("../../controllers/ux/ux.controller")

//Cuando el usuario accede a través de la url a una ruta se utiliza el método get. Éste recibe dos parámetros, la ruta a la que acceden (string), y la función del controlador que se va a ejecutar (callback)

//Rutas
router.get('/home', controller.home)
router.get('/colors', controller.colors)
router.get('/tipography', controller.tipography)
router.get('/buttons', controller.buttons)
router.get('/cards', controller.cards)
router.get('/lists', controller.lists)
router.get('/forms', controller.forms)
router.get('/tabs', controller.tabs)
router.get('/galleries', controller.galleries)
router.get('/icons', controller.icons)
router.get('/chat', controller.chat)

module.exports = router;