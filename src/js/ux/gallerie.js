//Guardamos el elemento galería en una constante
const gallery = document.getElementById('gallery')

//Si galería existe
if (gallery) {
    //Creamos una función para controlar la colocación de las imágenes. Recibe tres parámetros, el elemento contenedor (galería), las imágenes que queremos colocar y el número de columnas que queremos hacer.
    const galleryLayout = (containerElem, itemsElems, columns) => {
        //Añadimos a la galería la clase galería (por si no la tuviera) y la clase que especifica el número de columnas que queremos
        containerElem.classList.add('gallery', `columns-${columns}`)
        //Creamos un array que guardará las imágenes de cada columna
        let columnsElements = []

        for (let i = 1; i <= columns; i++) {
            //Por cada columna creamos un div vacío que será nuestra columna para colocar imágenes
            const column = document.createElement('div')
            //Añadimos esa columna a la galería
            containerElem.appendChild(column)
            //Metemos en el array la columna
            columnsElements.push(column)
        }

        //Bucle que calcula el número de imágenes entre las columnas para saber cuantas vueltas tiene que dar
        for (let m = 0; m < Math.ceil(itemsElems.length / columns); m++) {
            //bucle que coloca las imágenes en sus respectivas columnas
            for (let n = 0; n < columns; n++) {
                //por cada vuelta si, por ejemplo, tuvieramos 3 columnas, se irán colocando en la posción 0-1-2 3-4-5...
                const item = itemsElems[m * columns + n]
                //a cada imagen le añadimos la clase gallery__item
                item.classList.add('gallery__item')
                //añadimos la imagen a la columna que corresponda
                columnsElements[n].appendChild(item)
            }
        }
    }
    //Ejecutamos la función pasándole: galería, todos los elementos que tengan la clase gallery__item, número de columnas
    galleryLayout(gallery, document.querySelectorAll('.gallery__item'), 4)
}