//Guardamos en una constante el elemento toggle-menú, que es el icono de hamburguesa
const toggleMenu = document.getElementById('toggle-menu')
//Guardamos en una constante el menú, que en esta ocasión estará situado en el sidebar, no el header como es habitual
const sidebar = document.getElementById('sidebar')

//Si existe el icono de menú
if (toggleMenu) {
    //Detectamos el click en el menú
    toggleMenu.addEventListener('click', (e) => {
        //Si han hecho click en el icono o en una de las barras
        if (e.target.id == 'toggle-menu' || e.target.parentElement.id == 'toggle-menu') {
            //añadimos la clase show al toggle para que se convierta en una X
            toggleMenu.classList.toggle('show')
            //añadimos la clase show al sidebar para que se muestre
            sidebar.classList.toggle('show')
            //añadimos la clase no-scroll al body para que no se pueda hacer scroll
            document.body.classList.toggle('no-scroll')
        }
    })
}