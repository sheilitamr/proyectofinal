//Guardamos todos los elementos que contengan la clase tabs__item. 
const tabs = Array.from(document.querySelectorAll('.tabs__item'))
//Guardamos todos los elementos que contengan la clase panels__item
const panels = Array.from(document.querySelectorAll('.panels__item'))

//como document.querySelectorAll devuelve un HTMLCollection necesitamos convertirlo en un array a través del método Array.from(elementos)

//Comprobamos que tenemos elementos, en ambos arrays
if (tabs.length > 0 && panels.length > 0) {
    //Detectamos el click a través de delegacion de eventos, es decir, detectamos el click en el padre y miramos en qué hijo hemos hecho click
    document.getElementById('tabs').addEventListener('click', (e) => {
        //Comprobamos que el click ha sido en un tab (pestaña)
        if (e.target.classList.contains('tabs__item')) {
            //Guardamos el índice de ese elemento en el array
            const index = tabs.indexOf(e.target)
            //Recorremos el array para quitarle a todos los tabs el modificador --active
            tabs.map(tab => tab.classList.remove('tabs__item--active'))
            //Le añadimos el modificador solo al elemento sobre el que hemos hecho click
            tabs[index].classList.add('tabs__item--active')

            //Recorremos el array para quitarle a todos los panels el modificador --active
            panels.map(panel => panel.classList.remove('panels__item--active'))
            //Le añadimos el modificador solo al elemento que corresponde a su pestaña correspondiente
            panels[index].classList.add('panels__item--active')
        }
    })
}
