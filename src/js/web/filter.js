//Elemento contenedor del filtro de categorías
const filter = document.getElementById('filter')

//Si existe el filtro
if (filter) {
    //Ponemos el filtro a la escucha del click
    filter.addEventListener('click', (e) => {
        //Si hemos hecho click sobre un elemento span
        if (e.target.tagName == 'SPAN') {
            //Guardamos todas las imágenes en un array
            const allImages = Array.from(document.querySelectorAll('.gallery__item'))
            //Si el data-tag de la etiqueta es distito de all
            if (e.target.dataset.filterTag != 'all') {
                //Recorremos todas las imágenes y las ocultamos
                allImages.map(img => img.style.display = "none")
                //Recorremos todas las imágenes y solo mostramos las que coincidan con la etiqueta pulsada
                allImages.map(img => img.dataset.tag == e.target.dataset.filterTag ? img.style.display = "block" : null)
                //Si hemos hecho click en all
            } else {
                //Recorremos todas las imágenes y las mostramos
                allImages.map(img => img.style.display = "block")
            }
        }
    })
}

//Elemento contenedor del filtro de categorías
const filterProfile = document.getElementById('filter-profile')

//Si existe el filtro
if (filterProfile) {
    //Guardamos todas las imágenes en un array
    const allImages = Array.from(document.querySelectorAll('.gallery__item'))
    console.log(allImages)
    //Ponemos el filtro a la escucha del click
    filterProfile.addEventListener('click', (e) => {
        //Si hemos hecho click sobre un elemento span
        if (e.target.tagName == 'P') {
            //Si el data-tag de la etiqueta es distito de all
            if (e.target.dataset.filterTag != 'all') {
                //Recorremos todas las imágenes y las ocultamos
                allImages.map(img => img.style.display = "none")
                //Recorremos todas las imágenes y solo mostramos las que coincidan con la etiqueta pulsada
                allImages.map(img => img.dataset.tag == e.target.dataset.filterTag ? img.style.display = "block" : null)
                //Si hemos hecho click en all
            } else {
                //Recorremos todas las imágenes y las mostramos
                allImages.map(img => img.style.display = "block")
            }
        }
    })
}