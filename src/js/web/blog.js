const blogs = document.getElementById('blogs')
const forum = document.getElementById('forum')

const newPostForms = Array.from(document.querySelectorAll('.forum'))
console.log(newPostForms);
if (newPostForms.length > 0) {
    newPostForms.map(newPost => newPost.style.display = 'none')
}

if (blogs) {
    blogs.addEventListener('click', (e) => {
        if (e.target.classList.contains('blog') || e.target.parentElement.classList.contains('blog')) {
            const blog = e.target.classList.contains('blog') ? e.target : e.target.parentElement
            const blogContent = Array.from(blog.childNodes)
            const blogOpen = createElement('DIV', ['blog-open'], '', {}, '')

            const blogTitle = createElement('H2', ['blog-open__title'], blogContent[0].textContent, {}, '')
            blogOpen.appendChild(blogTitle)

            const blogImage = createElement('IMG', ['blog-open__img'], '', { src: blogContent[1].src }, '')
            blogOpen.appendChild(blogImage)

            const blogDescription = createElement('P', ['blog-open__description'], blogContent[2].textContent, {}, '')
            blogOpen.appendChild(blogDescription)

            const blogDate = createElement('TIME', ['blog-open__date'], blogContent[3].textContent, {}, '')
            blogOpen.appendChild(blogDate)

            const buttonBack = createElement('A', ['blog-open__link', 'button', 'button--cta'], 'Back to blogs', { href: '' }, '')
            blogOpen.appendChild(buttonBack)

            buttonBack.addEventListener('click', () => location.reload)

            blogs.textContent = ''
            blogs.classList.remove('blogs')
            blogs.classList.add('blogs--open')

            blogs.appendChild(blogOpen)
            showForum(e.target.parentElement.dataset.id)
            if (newPostForms.length > 0) {
                document.getElementById(`forum-${e.target.parentElement.dataset.id}`).style.display = 'block'
            }
        }
    })
}

const showForum = (id) => {
    //Hacemos una petición para obtener los datos de esa imagen pasándole como parámetro el id de la imagen
    fetch(`/web/getPosts/${id}/`)
        .then(response => (response.ok) ? Promise.resolve(response) : Promise.reject(new Error('Failed to load')))
        .then(response => response.json())
        .then(posts => {
            console.log(posts)
            if (posts.length > 0) {
                const fragment = document.createDocumentFragment()
                for (const post of posts) {
                    const postElement = createElement('DIV', ['post'], '', {}, '')

                    const postTitle = createElement('H2', 'post__title', post.title, {}, '')
                    postElement.appendChild(postTitle)

                    const postText = createElement('P', 'post__description', post.text, {}, '')
                    postElement.appendChild(postText)

                    const postFooter = createElement('DIV', 'post__footer', '', {}, '')
                    postElement.appendChild(postFooter)

                    const postDate = createElement('TIME', 'post__date', new Date(post.date).toLocaleDateString().split("-").reverse().join("-"), {}, '')
                    postFooter.appendChild(postDate)

                    const postUser = createElement('P', 'post__user', post.user, {}, '')
                    postFooter.appendChild(postUser)

                    fragment.appendChild(postElement)
                }
                forum.appendChild(fragment)
            }
        })
}