//Función para crear elementos de una forma más sencilla
//Recibe como parámetro: El elemento que queremos crear, sus clases :array, su contenido :string, atributos :object, su id :string
const createElement = (element, classes, textContent, attributes, id) => {
    //Guardamos el elemento en una constante
    const newElement = document.createElement(element)
    //Si tenemos clases recorremos el array y se las añadimos al elemento
    if (classes.length > 0) {
        for (const className of classes) {
            newElement.classList.add(className)
        }
    }

    //Para saber si tenemos atributos utilizamos el método Object.keys(object) que nos devuleve un array con las keys del objeto
    if (Object.keys(attributes).length > 0) {
        //Si tiene atributos recorremos el objeto asignando las keys como atributo y el valor accediendo a su valor como si fuera un array
        for (const attribute in attributes) {
            newElement.setAttribute(attribute, attributes[attribute])
        }
    }

    //Si tiene contenido se lo añadimos
    if (textContent) newElement.textContent = textContent

    //Si tiene id se lo añadimos
    if (id) newElement.id = id

    //Devolvemos el nuevo elemento
    return newElement
}

//Función para crear galerías. Recibe los datos de la consulta y el número de columnas, si no le dice número de columnas, por defecto es 5
const createGallery = (data, columns = 5) => {
    const gallery = document.getElementById('gallery')
    //Si existe la galería
    if (gallery) {
        //Creamos un fragmento de código
        const fragment = document.createDocumentFragment()
        //Recorremos cada una de las imágenes en data
        for (const img of data) {
            //Creamos un elemento imagen
            const imgElement = document.createElement('IMG')
            //Asignamos la ruta de la imagene
            imgElement.setAttribute('src', img.path)
            //Añadimos la clase gallery__item
            imgElement.classList.add('gallery__item')
            //Guardamos en un atributo data la etiqueta de la imagen
            imgElement.dataset.tag = img.tag
            //Le asignamos el id
            imgElement.dataset.id = img.id
            //Añadimos la imagen al fragmento de código
            fragment.appendChild(imgElement)
        }
        //Añadimos el fragmento de código con todas las imágenes a la galería
        gallery.appendChild(fragment)
        //Creamos la gallería con la función gallery layout
        galleryLayout(gallery, document.querySelectorAll('.gallery__item'), columns)
        //Añadimos un evento de escucha a la galería para saber en qué imagen hacemos click
        gallery.addEventListener('click', (e) => {
            //Llamamos a la función openImageLightbox pasandole la imagen en la que hemos hecho click
            openImageLightbox(e.target)
        })
    }
}

//Creamos una función que recibe: el contenedor de la etiqueta, los elementos de la galería y el número de columnas de la galería
const galleryLayout = (containerElem, itemsElems, columns) => {
    //Añadimos a la galería la clase galery y la clase colums seguido del número de columnas
    containerElem.classList.add('gallery', `columns-${columns}`)
    //Creamos un array para guardar los elementos de la galería
    let columnsElements = []

    //Por cada columna añadimos un div vacío que será nuestra columna
    for (let i = 0; i < columns; i++) {
        let column = document.createElement('div')
        containerElem.appendChild(column)
        columnsElements.push(column)
    }

    //Calculamos cuantas columnas necesitamos dividiendo el total de imágenes por el número de columnas redondeando hacia arriba
    for (let m = 0; m < Math.ceil(itemsElems.length / columns); m++) {
        //por cada columna añadimos a los divs vacíos cada una de las fotos de izquierda a derecha
        for (let n = 0; n < columns; n++) {
            let item = itemsElems[m * columns + n]
            if (item) {
                columnsElements[n].appendChild(item)
                item.classList.add('gallery__item')
            }
        }
    }
}