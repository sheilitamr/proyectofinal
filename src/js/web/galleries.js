//Condicional para saber si estamos en home
if (location.pathname.includes('home')) {
    //Evento para saber cuando ha cargado la página
    addEventListener('load', () => {
        //Petición para obtener las fotos de la galería principal
        fetch(`/web/getHomeGallerie`)
            //Si todo va bien devuelvemos una promesa, si algo falla lanzamos un error
            .then(response => (response.ok) ? Promise.resolve(response) : Promise.reject(new Error('Failed to load')))
            //Convertimos la promesa de respuesta a formato json
            .then(response => response.json())
            //trabajamos con los datos que nos devolvió la petición
            .then(data => {
                createGallery(data)
            })
    })
} else if (location.pathname.includes('profile')) {
    const user = location.pathname.substring(location.pathname.lastIndexOf('/') + 1)
    //Evento para saber cuando ha cargado la página
    addEventListener('load', () => {
        //Petición para obtener las fotos de la galería principal
        fetch(`/web/getUserGalleries/${user}`)
            //Si todo va bien devuelvemos una promesa, si algo falla lanzamos un error
            .then(response => (response.ok) ? Promise.resolve(response) : Promise.reject(new Error('Failed to load')))
            //Convertimos la promesa de respuesta a formato json
            .then(response => response.json())
            //trabajamos con los datos que nos devolvió la petición
            .then(data => {
                if(data.length>0)
                    if(data.length<5) createGallery(data,data.length)
                    else createGallery(data,5)
                else console.log('No hay galerías')
            })
    })
}

//Función que abre una ventana modal con todos los datos de la imagen
const openImageLightbox = (img) => {
    //Hacemos una petición para obtener los datos de esa imagen pasándole como parámetro el id de la imagen
    fetch(`/web/getImageInfo/${img.dataset.id}`)
        .then(response => (response.ok) ? Promise.resolve(response) : Promise.reject(new Error('Failed to load')))
        .then(response => response.json())
        .then(data => {
            //Añadimos la clase lightbox--show a la ventana modal
            document.getElementById('img-modal').classList.add('lightbox--show')
            //Escribimos el título de la imagen
            document.getElementById('img-info-title').textContent = data.title
            //Añadimos la ruta de la imagen
            document.getElementById('img-info-img').setAttribute('src', data.path)
            //Añadimos al enlace del usuario el nombre de usuario para poder visitar su perfil
            document.getElementById('img-user').href += data.user
            //Escribimos el nombre de usuario
            document.getElementById('img-user').textContent = data.user
            //Escribimos la descripción de la foto
            document.getElementById('img-info-description').textContent = data.description
            //Creamos un array para las etiquetas (aún no se pueden añadir varias)
            const tags = []
            //Creamos un elemento span que será nuestra etiqueta
            const tag = document.createElement('SPAN')
            //Le añadimos la clase img-ifo__tag
            tag.classList.add('img-info__tag')
            //Escribimos el nombre de la etiqueta
            tag.textContent = data.tag
            //Añadimos al array de etiquetas la etiqueta
            tags.push(tag)
            //Vaciamos el div de etiquetas por si se abre la misma imagen más de una vez que no se repita la etiqueta
            document.getElementById('img-info-tags').innerHTML = ''
            //Por cada etiqueta añadimos la etiqueta al div de etiquetas
            for (const tag of tags) {
                document.getElementById('img-info-tags').appendChild(tag)
            }
        })
}

