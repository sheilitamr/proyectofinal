const tabs = Array.from(document.querySelectorAll('.tabs__item'))
const panels = Array.from(document.querySelectorAll('.panels__item'))

//Comprobamos si el numero de tabs y panels es mayor de 0
if (tabs.length > 0 && panels.length > 0) {
    //Ponemos a la escucha de un click al elemento que tiene todas las pestañas
    document.getElementById('tabs').addEventListener('click', (e) => {
        //Si el click se hace en una pestaña
        if (e.target.classList.contains('tabs__item')) {
            //Guardamos la posición del tab 0,1,2...
            const index = tabs.indexOf(e.target)
            //Recorremos todas las pestañas y les quitamos la clase tabs__item--active
            tabs.map(tab => tab.classList.remove('tabs__item--active'))
            //Le añadimoc la clase tabs__item--active solo a la pestaña en la que hemos hecho click
            tabs[index].classList.add('tabs__item--active')

            //Repetimos el mismo proceso con los paneles
            panels.map(panel => panel.classList.remove('panels__item--active'))
            panels[index].classList.add('panels__item--active')
        }
    })
}
