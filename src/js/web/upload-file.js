//Formulario de subida de imagen
const uploadFileForm = document.getElementById('upload-file-form')
//Input del formulario
const fileInput = document.getElementById('file-input')
//Etiqueta del formulario
const fileLabel = document.getElementById('file-label')
//Imagen que mostrará el preview de la imagen que subimos
const fileImg = document.getElementById('file-img')
//Contenedor de los botones save/cancel
const uploadButtons = document.getElementById('upload-buttons')
//Contenedor de la información de subida
const uploadInfo = document.getElementById('upload-info')

//Si los elementos existen
if (uploadFileForm && fileInput) {
    //Ponemos a la excucha al formulario para detectar el click
    uploadFileForm.addEventListener('click', (e) => {
        //Si donde hacemos click no es un botón, fingimos un click en el boton de subir imagen
        if (!e.target.classList.contains('button')) fileInput.click()
    })
    
    //Ponemos a la escucha al campo file para saber cuando hemos cargado una imagen 
    fileInput.addEventListener('change', (e) => {
        //Eliminamos el texto "upload"
        fileLabel.textContent = ''
        //En la imagen de muestra cargamos el src para mostrar la imagen subida
        fileImg.src = URL.createObjectURL(e.target.files[0])
        //Mostramos los botones
        uploadButtons.style.display = 'flex'
        //Mostramos el contenedor de información
        uploadInfo.style.display = 'block'
    })

    //Ocultamos los botones y el contenedor de los datos de subida
    uploadButtons.style.display = 'none'
    uploadInfo.style.display = 'none'
}

if (uploadButtons) {
    //Ponemos a la escucha al contenedor de los botones
    uploadButtons.addEventListener('click', (e) => {
        //prevenimos el comportamiento por defecto
        e.preventDefault()
        //Si hemos hecho click en un boton
        if (e.target.classList.contains('button')) {
            //Si el boton es save
            if (e.target.id == 'save-upload') {
                //Enviamos el formulario
                uploadFileForm.parentElement.submit()
            }
            //Si el botón es cancel
            else if (e.target.id == 'cancel-upload') {
                //Volvemos a escribir el texto upload
                fileLabel.textContent = 'upload'
                //Quitamos el src a la foto de preview
                fileImg.src = ''
                //Ocultamos los botones
                uploadButtons.style.display = 'none'
                //Ocultamos el contenedor de los datos de subida
                uploadInfo.style.display = 'none'
            }
        }
    })
}