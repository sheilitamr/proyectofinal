//Elemento del chat completo
const chatContainer = document.getElementById('chat-container')
//Botón izquierdo que abre o cierra el chat
const chatIcon = document.getElementById('chat-icon')
//Input para escribir el nuevo mensaje
const chatInput = document.getElementById('new-message-form')

//Si existe el icono de chat
if (chatIcon) {
    //Cargamos la librería socket.io
    const socket = io()
    //Ponemos al icono a la ecucha del click
    chatIcon.addEventListener('click', (e) => {
        //Si hemos hecho click en el div o en el icono
        if (e.target.id == 'chat-icon' || e.target.tagName == 'I') {
            //Le quitamos todas las clases al icono
            chatIcon.firstElementChild.removeAttribute('class')
            //Si el contenedor del chat tiene la clase chat-container--show
            if (chatIcon.parentElement.classList.contains('chat-container--show')) {
                //Le quitamos la clase
                chatIcon.parentElement.classList.remove('chat-container--show')
                //Ponemos un icono de comentarios
                chatIcon.firstElementChild.classList.add('far', 'fa-comments')
                //Si el contenedor no tiene la clase chat-container--show
            } else {
                //le añadimos la clase chat-container--show
                chatIcon.parentElement.classList.add('chat-container--show')
                //Ponemos un icono de cerrar
                chatIcon.firstElementChild.classList.add('fa', 'fa-times')
            }
        }
    })


    chatInput.addEventListener('submit', e => {
        e.preventDefault()
        const data = {
            user: chatInput.parentElement.parentElement.dataset.user,
            date: new Date().toLocaleString().substring(0, new Date().toLocaleString().lastIndexOf(':')),
            message: e.target['new-message'].value
        }
        if (data.user.length > 0 && data.message.length > 0) {
            socket.emit('new message', data)
            createMessage(data, 'own')
        }
    })


    socket.on('new message', data => {
        if (data.user != 'admin' && data.user != 'server') createMessage(data, 'other')
        else if (data.user == 'admin') createMessage(data, 'admin')
        else createMessage(data, 'server')
    })
}

const createMessage = (data, user) => {

    let newMessage
    if (user != 'server') {
        newMessage = createElement('DIV', ['messages__item', `messages__item--${user}`], '', {}, '')
        const messageInfo = createElement('DIV', ['messages__info'], '', {}, '')
        messageInfo.appendChild(createElement('SPAN', ['messages__user'], data.user, {}, ''))
        messageInfo.appendChild(createElement('SPAN', ['messages__date'], data.date, {}, ''))
        const messageText = createElement('P', ['messages__message'], data.message, {}, '')
        newMessage.appendChild(messageInfo)
        newMessage.appendChild(messageText)
    } else if (user == 'admin') {
        newMessage = createElement('DIV', ['messages__item', `messages__item--admin`], '', {}, '')
        const messageInfo = createElement('DIV', ['messages__info'], '', {}, '')
        messageInfo.appendChild(createElement('SPAN', ['messages__user'], data.user, {}, ''))
        messageInfo.appendChild(createElement('SPAN', ['messages__date'], data.date, {}, ''))
        const messageText = createElement('P', ['messages__message'], data.message, {}, '')
        newMessage.appendChild(messageInfo)
        newMessage.appendChild(messageText)
    } else {
        newMessage = createElement('P', ['messages__item', `messages__item--server`], data.message, {}, '')
    }
    printMessage(newMessage)
}


const messagesElement = document.getElementById('messages')
const printMessage = (message) => {
    messagesElement.appendChild(message)
    chatInput.reset()
    chatInput.childNodes[0].value = ''
    messagesElement.scrollTo(0, messagesElement.scrollHeight)
}