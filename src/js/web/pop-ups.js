//Localizamos el elemento que mostrará el mensaje
const popUp = document.getElementById('pop-up')

//Función para crear el mensaje. Recibe el mensaje, el color de fondo y el color de la letra, por defecto blanco
const showPopUp = (msg, bg, color = '#fff') => {
    //Añadimos el mensaje
    popUp.textContent = msg
    //Añadimos el color de fondo
    popUp.style.backgroundColor = bg
    //Añadimos el color de la letra
    popUp.style.color = color
    //Añadimos el modificador --animate
    popUp.classList.add('pop-up--animate')
    //Escuchamos el evento de animación terminada
    popUp.addEventListener('animationend', () => {
        //Quitamos el modificador --animate
        popUp.classList.remove('pop-up--animate')
    })
}

//Variaciones del mensaje del hash
if (location.hash == '#registered') {
    showPopUp('Usuario registrado', 'var(--primary-color)')
} else if (location.hash == '#login') {
    showPopUp('Sesión iniciada', 'var(--primary-color)')
} else if (location.hash == '#close') {
    showPopUp('Sesión cerrada', 'var(--danger-color)')
} else if (location.hash == '#error') {
    showPopUp('Usuario o password incorrecto', 'var(--danger-color)')
}