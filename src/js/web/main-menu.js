const toggleMenu = document.getElementById('toggle-menu')
const sidebar = document.getElementById('sidebar')

if (toggleMenu) {
    //Detectamos el click en el menú
    toggleMenu.addEventListener('click', (e) => {
        //Si han hecho click en el icono o en una de las barras
        if (e.target.id == 'toggle-menu' || e.target.parentElement.id == 'toggle-menu') {
            //añadimos la clase show al toggle para que se convierta en una X
            toggleMenu.classList.toggle('show')
            //añadimos la clase show al sidebar para que se muestre
            sidebar.classList.toggle('show')
            //añadimos la clase no-scroll al body para que no se pueda hacer scroll
            document.body.classList.toggle('no-scroll')
        }
    })
}