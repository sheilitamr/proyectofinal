// Guardamos los elementos que abren la ventana modal de Login y de Register.
const openRegisterForm = document.getElementById('open-register-modal')
const openLoginForm = document.getElementById('open-login-modal')

//Guardamos los formularios
const registerModal = document.getElementById('register-modal')
const loginModal = document.getElementById('login-modal')

// Guardamos todos los lightboxs que hay en la página para que cuando pulsemos sobre ellos, se cierren.
const lightboxs = [...document.querySelectorAll('.lightbox')]


//Comprobamos que existe el elemento
if (openRegisterForm) {
    //Evento de escucha para abrir el formulario de registro
    openRegisterForm.addEventListener('click', () => {
        //Quitamos la clase al login por si estuviera abierto
        loginModal.classList.remove('lightbox--show')
        //Añadimos la clase ligbox--show al registro para que se muestre
        registerModal.classList.add('lightbox--show')
    })
}

//Comprobamos que existe el elemento
if (openLoginForm) {
    //Evento de escucha para abrir el formulario de registro
    openLoginForm.addEventListener('click', () => {
        //Quitamos la clase al register por si estuviera abierto
        registerModal.classList.remove('lightbox--show')
        //Añadimos la clase ligbox--show al login para que se muestre
        loginModal.classList.add('lightbox--show')
    })
}

//Añadimos un evento de escucha a todos los lightbox para que al detectar el click quitemos la clase lightbox--show para ocultarlo
lightboxs.map(el => {
    el.addEventListener('click', e => {
        if (e.target.classList.contains('lightbox')) {
            e.target.classList.remove('lightbox--show')
        }
    })
})

//Login
const loginForm = document.getElementById('login-form')

//Register
const registerForm = document.getElementById('register-form')
registerForm.addEventListener('submit', (e) => {
    e.preventDefault()
    //VALIDATE
    e.target.submit()
})