const contestTime = document.getElementById('contest-remaining')

if (contestTime) {
    const fechaInicio = new Date('2016-07-12').getTime();
    const fechaFin = new Date('2016-08-01').getTime();

    const diff = fechaFin - fechaInicio;

    contestTime.innerHTML = `Faltan ${diff / (1000 * 60 * 60 * 24)} días`
}