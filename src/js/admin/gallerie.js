const gallery = document.getElementById('gallery')

if (gallery) {
    const galleryLayout = (containerElem, itemsElems, columns) => {
        containerElem.classList.add('gallery', `columns-${columns}`)
        let columnsElements = []

        for (let i = 1; i <= columns; i++) {
            let column = document.createElement('div')
            containerElem.appendChild(column)
            columnsElements.push(column)
        }

        for (let m = 0; m < Math.ceil(itemsElems.length / columns); m++) {
            for (let n = 0; n < columns; n++) {
                let item = itemsElems[m * columns + n]
                item.classList.add('gallery__item')
                columnsElements[n].appendChild(item)
            }
        }
    }
    galleryLayout(gallery, document.querySelectorAll('.gallery__item'), 4)
}