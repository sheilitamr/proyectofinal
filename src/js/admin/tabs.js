const tabs = Array.from(document.querySelectorAll('.tabs__item'))
const panels = Array.from(document.querySelectorAll('.panels__item'))

if (tabs.length > 0 && panels.length > 0) {
    document.getElementById('tabs').addEventListener('click', (e) => {
        if (e.target.classList.contains('tabs__item') && e.target.id != 'update-playlist' && e.target.id != 'update-tournament') {
            const index = tabs.indexOf(e.target)
            tabs.map(tab => tab.classList.remove('tabs__item--active'))
            tabs[index].classList.add('tabs__item--active')

            panels.map(panel => panel.classList.remove('panels__item--active'))
            panels[index].classList.add('panels__item--active')
        }
    })
}
