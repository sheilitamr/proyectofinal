const blogs = document.getElementById('blogs')

if (blogs) {
    blogs.addEventListener('click', (e) => {
        if (e.target.tagName == 'I') {
            if (e.target.classList.value.indexOf('pen') !== -1) {
            } else if (e.target.classList.value.indexOf('trash') !== -1)
                location.href = `/admin/deleteBlog/${e.target.parentElement.parentElement.dataset.id}`
        }
    })
}