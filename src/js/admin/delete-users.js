const users = document.getElementById('users')

if(users){
    users.addEventListener('click', (e)=>{
        if(e.target.tagName == 'I') location.href = `/admin/deleteUser/${e.target.parentElement.dataset.id}`
    })
}