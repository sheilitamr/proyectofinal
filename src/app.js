const express = require('express')
const path = require('path')

// Intializations
const app = express()

// Settings
app.set('port', process.env.PORT || 80)
app.set("view engine", "pug")
app.set('views', path.join(__dirname, '/views'))

// Middlewares
//app.use(morgan('dev'));
app.use(express.urlencoded({ extended: true }))

//INDEX
app.use(require('./routes/index.routes'))
//UX
app.use('/ux', require('./routes/ux/ux.routes'))
//ADMIN
app.use('/admin', require('./routes/admin/admin.routes'))
//WEB
app.use('/web', require('./routes/web/navigation.routes'))
app.use('/web', require('./routes/web/user.routes'))
app.use('/web', require('./routes/web/galleries.router'))
app.use('/web', require('./routes/web/forum.router'))
app.use('/web', require('./routes/web/uploads.routes.js'))


//Errors
app.use((err, req, res, next) => {
  res.send({ error: err.message })
})

// Public
app.use(express.static(path.join(__dirname, '../public')))

//start server
const server = app.listen(app.get("port"), () => {
  console.log(`Server on port ${app.get("port")}`)
})

//socket.io
const socketIO = require("socket.io")
module.exports.io = socketIO.listen(server)
require('./sockets/sockets')