const { io } = require('../app')
const path = require('path')

io.on('connection', socket => {
    console.log('new connection')
    const dirname = path.dirname(socket.handshake.headers.referer)
    const username = dirname.substring(dirname.lastIndexOf('/') + 1)
    const date = new Date().toLocaleTimeString()
    socket.broadcast.emit('new message', {
        user: 'server',
        message: `${username} is online at ${date} `
    })

    socket.on('new message', (data) => {
        socket.broadcast.emit('new message', data)
        console.log(data);
    })

    socket.on('disconnect', () => {
        socket.broadcast.emit('new message', {
            user: 'server',
            message: `${username} is offline at ${date} `
        })
    })
})